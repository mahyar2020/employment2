<?php

use Illuminate\Support\Facades\Route;


Route::prefix('v1')->namespace('Api\v1')->group(function() {
    Route::post('register', 'UserController@register');
    Route::post('login', 'UserController@login');
    Route::middleware('auth:api')->group(function () {
        Route::post('update/{user}', 'UserController@update');
        Route::get('user', 'UserController@getUser');
    });

});
