<?php

namespace App\Http\Requests\User;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string',
            'last_name'  => 'required|string',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'mobile_number' => 'required|string|min:11|max:11',
            'image_url' => 'nullable|mimes:jpeg,bmp,png|max:10240',
            'gender' => 'required|int|'.Rule::in([User::GENDER_MALE, User::GENDER_FEMALE, User::GENDER_OTHER])
        ];
    }
}
