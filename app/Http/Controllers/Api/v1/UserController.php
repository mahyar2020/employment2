<?php

namespace App\Http\Controllers\Api\v1;

use App\User;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\RegisterRequest;
use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Http\Resources\v1\GetUserResource;
use App\Http\Resources\v1\UserResource;



class UserController extends Controller
{
    public function register(RegisterRequest $request)
    {
        $validData = $request->validated();

        $user =new User();
        $user['first_name'] = $validData['first_name'];
        $user['last_name'] = $validData['last_name'];
        $user['mobile_number'] = $validData['mobile_number'];
        $user['email'] = $validData['email'];
        $user['gender'] = $validData['gender'];
        $user['password'] = bcrypt($validData['password']);
        $user['api_token'] = Str::random(100);

        if ($request->hasFile('image_url')) {
            $file = $validData['image_url'];
            $imagePath = "/upload/images";
            $filename = time().$file->getClientOriginalName();
            $file->move(public_path($imagePath) , $filename);
            $user['image_url'] = url("{$imagePath}/{$filename}");
        }

        if (! $user->save()) {
            return response([
                'data' => [],
                'massage' => ['error to save data']
            ], 403);
        }

        return response([
            'data' => [],
            'massage' => ['success']
        ], 201);

    }

    public function login(LoginRequest $request)
    {
        $validData = $request->validated();

        if(! auth()->attempt($validData)) {
            return response([
                'data' => 'Wrong information',
                'status' => 'error'
            ],403);
        }

        auth()->user()->update([
            'api_token' => Str::random(100)
        ]);

        return new UserResource(auth()->user());
    }

    public function update(UpdateRequest $request, User $user)
    {
        $validData = $request->validated();

        $user->first_name = $validData['first_name'];
        $user->last_name = $validData['last_name'];
        $user->password = bcrypt($validData['password']);
        $user->mobile_number = $validData['mobile_number'];
        $user->gender = $validData['gender'];

        if ($request->hasFile('image_url')) {
            $file = $validData['image_url'];
            $imagePath = "/upload/images";
            $filename = time().$file->getClientOriginalName();
            $file->move(public_path($imagePath) , $filename);
            $url = 'http://localhost:8000/';
            if ($user->image_url) {
                $old_url = str_replace($url,'', $user->image_url);
                @unlink($old_url);
            }
            $user->image_url = url("{$imagePath}/{$filename}");

        }

        if (!$user->save()) {
            return response([
                'data' => [],
                'massage' => ['error to save data']
            ], 403);
        }

        return response([
            'data' => [],
            'massage' => ['success']
        ], 201);

    }

    public function getUser()
    {
        return new GetUserResource(auth()->user());
    }
}
